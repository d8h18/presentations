+ installier auf deinem Handy, PC und tablet Firefox. (Gibts nicht für iPhone, iPad und mac)
+ geh in die Einstellungen von Firefox unter "suche" und stelle die Suchmaschine um auf "DuckDuckGo" (das liefert genauso gute Ergebnisse wie Google, sammelt aber keine Daten)
+ geh in die Einstellungen von Firefox unter "Addons" und installiere "uBlock origin" (das ist ein Werbeblocker der verhindert, dass Internetseiten Informationen über euch sammeln)
+ deinstalliere Chrome


["Warum sollte ich nicht Chrome benutzen?"](https://contrachrome.com/comic/279/)



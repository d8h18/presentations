## Mit alternativen Frontends kann man datensammelnde Services datenschutz-freundlicher nutzen
(Man benutzt den ursprünglichen Dienst zB Youtube, aber durch einen "Mittelsmann" bekommt bspw Google keine/weniger Daten während der Nutzung)

### Youtube:
+ https://invidious.snopyta.org
+ (noch besser: Peertube anstatt YouTube mit https://sepiasearch.org/)

### Twitter:
+ https://nitter.snopyta.org
+ (noch besser: Mastodon https://codeberg.org/d8h18/presentations/src/branch/main/Mastodon)

### Reddit:
+ https://teddit.net
+ (noch besser: Lemmy anstatt Reddit mit https://join-lemmy.org/)

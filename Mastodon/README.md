# Mastodon Präsentation

## Vorwort
+ manche Begriffe sind technisch. Mastodon kann auch genutzt werden, ohne alle technischen Begriffe zu verstehen.

## Schnellanleitung (für Ungeduldige):
+ Mastodon-App herunter laden (optional)
+ auf Climatejustice.social account erstellen
+ Vorstellungs-post mit "#neuhier" hashtag und Interessen
+ über hashtag-suche accounts finden, denen man folgen kann
+ über https://mastodir.de/ accounts finden, denen man folgen kann


## Was ist Mastodon
+ Microblogging Dienst (wie twitter).
	+ teilen von kurzen Texten (500 Zeichen oder individuell), Bilder, Videos
	+ folgen von anderen accounts, um deren Inhalte in Timeline zu bekommen
+ föderiert (vernetzte Server wie bei E-Mail)
+ Teil des Fediverse (föderiertes Universum)
	+ -> föderierte/vernetzte Alternativen:
		+ Twitter (Mastodon, https://joinmastodon.org/de)
		+ facebook (friendica, https://friendi.ca/)
		+ Instagram (https://pixelfed.org/)
		+ YouTube (peertube https://sepiasearch.org/)
		+ Reddit (https://join-lemmy.org/)
	+ -> Posts von Pixelfed-account können mit Mastodon abonniert werden
	+ https://www.kuketz-blog.de/das-fediverse-unendliche-weiten-als-schaubild-diagramm/

## Unterschied zu Twitter
+ Teil eines größeren Netzwerks
+ keine Werbung
+ lineare timeline anstatt algorithmisch (alle Posts von gefolgten Accounts/hashtags werden angezeigt, und nur diese)
	+ man verpasst keine posts
	+ Algorithmus von Twitter/FB/Insta ist darauf trainiert, "Geld zu verdienen"
		+ -> Algorithmus favorisiert Hetze, Fakenews, polarisierenden Inhalt
+ in der Hand von Individuen und Organisationen
+ jeder kann seinen eigenen Server betreiben
+ 3 Timelines:
	+ Home: gefolgte Accounts/hashtags
	+ Lokal: alle Posts von eigener Instanz
	+ Föderiert: alle Posts von allen "bekannten" Instanzen
+ Handle (accountname):
	+ Twitter: @KL_Deutschland
	+ Mastodon: @Klimaliste_Deutschland@climatejustice.social


## Wie melde ich mich an
+ Dienst auswählen (Mastodon, pixelfed...)
+ Instanz finden
	+ https://joinmastodon.org/de/servers
	+ Bsp:
		+ https://climatejustice.global (gruppen)
		+ https://climatejustice.social/ (individuen)
		+ https://freiburg.social/
		+ https://sueden.social
	+ (man kann von einer Instanz zu einer anderen umziehen und seine Follower und Follows mit nehmen)
+ app installieren
	+ https://joinmastodon.org/de/apps
		+ nur persönliche Timeline und einfache Bedienung: Mastodon app
		+ alle TLs, multi-account: Tusky (nur Android)
		+ apps zB von https://f-droid.org/ (privatsphäre-freundlicher Appstore für android)

## Wie finde ich interessante accounts?
+ https://mastodir.de/ (deutsch)
+ https://fediverse.info/explore/people
+ https://fedi.directory/
+ nach hashtags suchen (hashtags folgen)
+ "#neuhier" Beitrag mit eigenen Interessen (und hashtags) erstellen
+ Twitter-Kontakte finden: 
	+ https://fedifinder.glitch.me/
	+ https://www.movetodon.org
	+ https://debirdify.pruvisto.org/

## Crossposting
+ Crossposting bedeutet, Posts automatisch zwischen Mastodon und Twitter zu spiegeln
+ geht in beide Richtungen
+ https://handbuch.gruene.social/#/mastodon/crossposting
## Eigene Instanz erstellen

+ Selbst hosten:
	+ bei entsprechenden Projekten auf homepage
+ Managed hosting:
	+ https://growyourown.services/grow-your-own-social-network/


## Interessante links:
+ info über Fediverse:
	+ [Was ist ein Föderiertes Netzwerk? (Video)](https://videos.im.allmendenetz.de/w/09718999-9718-4d01-b9a7-979be3e717a5)
	+ [Fediverse.info](https://fediverse.info/)
	+ [Was ist das Fediverse (Video)(Englisch)](https://tilvids.com/w/7ec57164-aef3-4861-a33b-61dd7629faac)
+ Anleitungen:
	+ https://www.kuketz-blog.de/tschuess-twitter-hallo-fediverse-hallo-mastodon/
	+ https://fedi.tips/ (englisch)
	+ https://thomas-ebinger.de/2022/11/meine-mastodon-tipps-und-tricks/
	+ https://contentnation.net/de/favstarmafia/artikel1
	+ https://handbuch.gruene.social/#/mastodon/README
	+ https://netzpolitik.org/2022/alternative-plattformen-so-klappt-der-umzug-auf-mastodon/
	+ https://digitalcourage.de/digitale-selbstverteidigung/fediverse
+ Artikel über Mastodon
	* [Was ist Mastodon? (Video) (Englisch)](https://tilvids.com/w/887e7b95-a6b1-4df1-83fd-d5c47f17ec4c)
	+ https://netzpolitik.org/2022/umzug-zu-mastodon-so-schwer-ist-es-nun-auch-nicht/
	+ https://netzpolitik.org/2022/twitter-exodus-wie-behoerden-medien-und-ngos-mastodon-fuer-sich-entdecken/
	+ https://axbom.com/fediverse/
	+ https://tilvids.com/w/887e7b95-a6b1-4df1-83fd-d5c47f17ec4c
